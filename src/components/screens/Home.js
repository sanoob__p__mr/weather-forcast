import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Dimensions,
  ScrollView,
  Image,
  Pressable,
} from 'react-native';
import React, {useState} from 'react';
import RNPickerSelect from 'react-native-picker-select';

let {width} = Dimensions.get('screen');
export default function Home() {
  let products = [
    {
      id: 1,
      name: 'Nike shoe',
      image:
        'https://static.nike.com/a/images/c_limit,w_592,f_auto/t_product_v1/3c20bb9e-48d2-44e4-bb2a-abe502b11b50/air-max-270-shoes-s1JpCx.png',
      price: 100,
    },
    {
      id: 2,
      name: 'G-Shock',
      image:
        'https://assets.myntassets.com/dpr_1.5,q_60,w_400,c_limit,fl_progressive/assets/images/10704088/2022/6/20/840f10de-0be7-446a-bfbc-2ed4edd798911655717642616-Casio-G-Shock-Men-Black-Analogue-and-Digital-watch-G976-GA-1-6.jpg',
      price: 200,
    },
    {
      id: 3,
      name: 'OTTO shirt',
      image: 'https://m.media-amazon.com/images/I/91sQs51s6kL._UL1500_.jpg',
      price: 700,
    },
    {
      id: 4,
      name: 'Samsung Watch',
      image:
        'https://images.samsung.com/in/galaxy-watch5-pro/feature/galaxy-watch5-pro-battery-display-mo.png',
      price: 500,
    },
    {
      id: 5,
      name: 'Nike shoe',
      image:
        'https://www.dizo.net/repository/image/1f5585d2-4168-4644-92e8-dba7bc0cd2bc.jpg',
      price: 150,
    },
  ];
  const [productList, setProductList] = useState(products);
  const [cartItems, setCartItems] = useState([]);
  const [total, setTotal] = useState(0);
  const addProduct = item => {
    let product = productList.find(element => {
      return element.id == item.id;
    });
    let exclude_list = productList.filter(element => {
      return element.id != item.id;
    });
    setTotal(total + item.price);
    setCartItems([...cartItems, {...product, cart_count: 1}]);
    setProductList(exclude_list);
  };

  const increment = item => {
    let list = [...cartItems];
    let index = cartItems.findIndex(element => {
      return element.id == item.id;
    });
    let count = item.cart_count + 1;
    list[index].cart_count = count;
    setTotal(total + item.price);
    setCartItems(list);
  };

  const decrement = item => {
    if (item.cart_count == 1) {
      let exclude_list = cartItems.filter(element => {
        return element.id != item.id;
      });
      setTotal(total - item.price);
      setCartItems(exclude_list);
      setProductList([...productList, item]);
    } else {
      let list = [...cartItems];
      let index = cartItems.findIndex(element => {
        return element.id == item.id;
      });
      let count = item.cart_count - 1;
      list[index].cart_count = count;
      setTotal(total - item.price);
      setCartItems(list);
    }
  };
  return (
    <View style={{flex: 1}}>
      <ScrollView style={styles.container}>
        {productList.map(item => (
          <View style={styles.cardItem}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image source={{uri: item.image}} style={styles.cartImage} />

              <Text style={{marginLeft: 10}}>{item.name}</Text>
              <Text style={{marginLeft: 10, fontWeight: 'bold'}}>
                ₹{item.price}
              </Text>
            </View>

            <Pressable onPress={() => addProduct(item)}>
              <Text style={styles.addButton}>Add</Text>
            </Pressable>
          </View>
        ))}
        <View style={styles.divide} />
        {cartItems.map(item => (
          <View style={styles.cardItem}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image source={{uri: item.image}} style={styles.cartImage} />

              <Text style={{marginLeft: 10}}>{item.name}</Text>
              <Text style={{marginLeft: 10, fontWeight: 'bold'}}>
                ₹{item.price}
              </Text>
            </View>

            <Pressable onPress={() => decrement(item)}>
              <Text style={styles.addButton}>-</Text>
            </Pressable>
            <Text>{item.cart_count}</Text>
            <Pressable onPress={() => increment(item)}>
              <Text style={styles.addButton}>+</Text>
            </Pressable>
          </View>
        ))}

        <View style={{height: 100}} />
      </ScrollView>
      <View style={styles.summary}>
        <Text style={styles.totalText}>Total - {total}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingVertical: '10%',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  details: {
    width: width - 60,
    marginHorizontal: 30,
    paddingVertical: 20,
    paddingHorizontal: 20,
    backgroundColor: 'rgba(0,0,0,.25)',
    borderRadius: 13,
  },
  text: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    marginBottom: 5,
  },
  cardItem: {
    height: 100,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 20,
    marginBottom: 10,
    overflow: 'hidden',
    paddingRight: 10,
    justifyContent: 'space-between',
  },
  cartImage: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
  },
  addButton: {
    backgroundColor: 'dodgerblue',
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 12,
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 8,
  },
  summary: {
    position: 'absolute',
    bottom: 0,
    paddingVertical: 20,
    paddingHorizontal: 30,
    borderTopWidth: 1,
    width: '100%',
    backgroundColor: '#fff',
  },
  totalText: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  divide: {
    height: 2,
    marginVertical: '5%',
    backgroundColor: '#999',
  },
});
