import {
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function LogIn() {
  const [confirm, setConfirm] = useState(null);
  const [code, setCode] = useState('');
  const [user, setUser] = useState();

  const [seconds, setSeconds] = useState(0);
  const [phone, setPhone] = useState('');

  // Handle the button press
  async function signInWithPhoneNumber(phoneNumber) {
    const confirmation = await auth().signInWithPhoneNumber(
      '+91' + phoneNumber,
    );
    setConfirm(confirmation);
  }

  async function confirmCode() {
    try {
      let res = await confirm.confirm(code);
      console.log(JSON.stringify(res));
    } catch (error) {
      console.log('Invalid code.');
    }
  }

  function onAuthStateChanged(user) {
    console.log('\n');
    console.log('===', user, 'user');
    setUser(user);
  }
  useEffect(() => {
    if (seconds > 0) {
      setTimeout(() => {
        setSeconds(seconds - 1);
      }, 1000);
    }
  }, [seconds]);

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    // auth().signOut();
    return subscriber; // unsubscribe on unmount
  }, []);

  const resentOtp = () => {
    setSeconds(10);
  };

  const logOut = () => {
    auth().signOut();
  };

  if (user) {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
        <Text>Home Page</Text>
        <View style={styles.row}>
          <Button title="Log out" onPress={() => logOut()} />
        </View>
      </View>
    );
  }

  if (!confirm) {
    return (
      <View style={styles.container}>
        <TextInput
          autoFocus={true}
          value={phone}
          onChangeText={setPhone}
          style={styles.textInput}
          placeholder={'Enter phone number'}
          keyboardType="number-pad"
        />
        <View style={styles.row}>
          <Button
            title="Sent Otp"
            onPress={() => signInWithPhoneNumber(phone)}
          />
        </View>
      </View>
    );
  }

  return (
    <>
      <TextInput value={code} onChangeText={text => setCode(text)} />
      <Button title="Confirm Code" onPress={() => confirmCode()} />
      <TouchableOpacity
        disabled={seconds != 0}
        activeOpacity={0.7}
        style={styles.button}
        onPress={resentOtp}>
        <Text>{seconds != 0 ? seconds : 'Resent Otp'}</Text>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  textInput: {
    borderWidth: 1,
    borderRadius: 8,
    width: '100%',
    marginBottom: 20,
    paddingHorizontal: 20,
  },
  row: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
  },
  button: {
    marginLeft: 20,
  },
});
